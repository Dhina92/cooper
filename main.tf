provider "aws" {
  region     = "us-west-2"
}

resource "aws_instance" "web" {
  ami           = "ami-04590e7389a6e577c"
  instance_type = "t2.micro"
  key_name      = "dhinan"
  tags = {
    Name = "CooperEC2"
  }
    provisioner "local-exec" {
    command = "echo ${aws_instance.web.public_ip} >> hosts.txt"
  }
}

resource "aws_lb_target_group_attachment" "WebAppURL" {
  target_group_arn = "arn:aws:elasticloadbalancing:us-west-2:286346748175:targetgroup/WebAppURL/e3fbcf032230b6da"
  target_id        = "${aws_instance.web.id}"
  port             = 8081
}

output "ip" {
  value = "${aws_instance.web.*.public_ip}"
}
