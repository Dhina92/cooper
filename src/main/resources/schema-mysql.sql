DROP TABLE IF EXISTS employee;

CREATE TABLE employee (
  empId VARCHAR(10) NOT NULL,
  empName VARCHAR(100) NOT NULL
);

INSERT INTO employee(empId,empName)values("001","Mohana Sundaram");
INSERT INTO employee(empId,empName)values("002","Senthil narayanan");
INSERT INTO employee(empId,empName)values("003","Madhan Babu");