import boto3
import urllib
import json
#import httplib
import socket
import requests
import urllib3
import datetime
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def lambda_handler(event, context):
    http = urllib3.PoolManager()
    cw = boto3.client('cloudwatch')
    now = datetime.datetime.now()
    time = now.strftime("%Y-%m-%d %H:%M")
    try:
        resp = http.request('GET','http://cooper-1758536345.us-west-2.elb.amazonaws.com/employees')
        print (resp.status)
        if resp.status == 200 or resp.status == 301 or resp.status == 302 or resp.status == 303:
            print "Working"
            cw.put_metric_data(Namespace='WebStatus',MetricData=[{'MetricName':'StatusCode','Dimensions': [{'Name': 'DomainName','Value': 'cooper'},],'Value': 1,},])
        else:
            print "Not-Working"
            cw.put_metric_data(Namespace='WebStatus',MetricData=[{'MetricName':'StatusCode','Dimensions': [{'Name': 'DomainName','Value': 'cooper'},],'Value': 0,},])
    except:
        pass
